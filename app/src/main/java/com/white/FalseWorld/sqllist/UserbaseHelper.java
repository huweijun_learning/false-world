package com.white.FalseWorld.sqllist;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UserbaseHelper extends SQLiteOpenHelper {
    /*
     * 数据库为user，保存在用户手机，用于自动登录
     * 数据库存储在用户手机中
     * */
    public  static final String CreateData="create table FalseWorld_user ("
            +"username text )";

    private Context mContext;

    public UserbaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super( context, name, factory, version );
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL( CreateData );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
