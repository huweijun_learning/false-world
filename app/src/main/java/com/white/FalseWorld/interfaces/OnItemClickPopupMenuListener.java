package com.white.FalseWorld.interfaces;

public interface OnItemClickPopupMenuListener {

    void onItemClickCopy(int position);

    void onItemClickTranslation(int position);

    void onItemClickHideTranslation(int position);

    void onItemClickCollection(int position);
}
