package com.white.FalseWorld.interfaces;

/**
 * @author KCrason
 * @date 2018/5/2
 */
public interface OnTimerResultListener {
    void onTimerResult();
}
