package com.white.FalseWorld.interfaces;

public interface OnPraiseOrCommentClickListener {
    void onRefresh();

    void onPraiseClick(int position);


    void onCommentClick(int position);
}
