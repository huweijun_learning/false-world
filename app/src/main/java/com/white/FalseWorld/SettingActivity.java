package com.white.FalseWorld;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gyf.immersionbar.ImmersionBar;
import com.white.FalseWorld.sqllist.UserbaseHelper;

public class SettingActivity extends AppCompatActivity {

    private LinearLayout jcbb,wxts,gnjs,zxkf,kfz;
    private ImageView setting_img_back;
    private Button quit_fw;
    private UserbaseHelper dbhelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_setting );
        ImmersionBar.with(this).init();//一键透明状态栏

        jcbb=findViewById( R.id.jcbb );
        wxts=findViewById( R.id.wxts );
        gnjs=findViewById( R.id.gnjs );
        zxkf=findViewById( R.id.zxkf );
        kfz=findViewById( R.id.kfz );
        setting_img_back=findViewById( R.id.setting_img_back );
        quit_fw=findViewById( R.id.quit_fw );

        setting_img_back.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        } );
        jcbb.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText( SettingActivity.this,"请关注[NoWhite]公众号，以便知晓新版本！",Toast.LENGTH_LONG ).show();
            }
        } );

        zxkf.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String urlqq = "mqqwpa://im/chat?chat_type=wpa&uin=1076091215&version=1&src_type=web&web_src=qq.com";
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlqq)));
                SettingActivity.this.overridePendingTransition(0, 0);
            }
        } );
        quit_fw.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        } );
    }

    private void showDialog() {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("温馨提示");
        builder.setMessage("退出后不会删除任何历史数据，下次登录依然可以使用本账号。(点任意地方可取消)");
        builder.setPositiveButton("取消",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.setPositiveButton("退出",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dbhelper = new UserbaseHelper(getApplicationContext(), "FalseWorld_user.db", null, 1);
                        SQLiteDatabase db = dbhelper.getWritableDatabase();
                        db.delete( "FalseWorld_user",null,null );
                        Intent intent =new Intent( SettingActivity.this,LoginActivity.class );
                        startActivity( intent );
                    }
                });
        AlertDialog dialog=builder.create();
        dialog.show();

    }

    @Override
    protected void onPause() {
        overridePendingTransition(0,0);
        super.onPause();
    }
}
