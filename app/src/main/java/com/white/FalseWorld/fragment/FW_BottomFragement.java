package com.white.FalseWorld.fragment;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextPaint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.white.FalseWorld.CreateNewsActivity;
import com.white.FalseWorld.IndexActivity;
import com.white.FalseWorld.LoginStepActivity;
import com.white.FalseWorld.R;
import com.white.FalseWorld.sqllist.UserbaseHelper;

/**
 * 作者：white
 * 时间：2020/10/16.
 *
 * APP底部导航
 */
public class FW_BottomFragement extends Fragment {
    private TextView dt,fx,wo;
    private UserbaseHelper dbhelper;
    String user;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated( savedInstanceState );
        dbhelper = new UserbaseHelper(getContext(), "FalseWorld_user.db", null, 1);
        SQLiteDatabase db = dbhelper.getWritableDatabase();
        Cursor cursor = db.query("FalseWorld_user", null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                user=cursor.getString(cursor.getColumnIndex("username")) ;
            } while (cursor.moveToNext());
        }
        cursor.close();
        dt.setOnClickListener( new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                //选中颜色加粗
                TextPaint paint = dt.getPaint();
                TextPaint paint_1 = fx.getPaint();
                TextPaint paint_2 = wo.getPaint();
                paint.setFakeBoldText(true);
                paint_1.setFakeBoldText( false );
                paint_2.setFakeBoldText( false );
                dt.setTextColor( Color.parseColor( "#1F7C21") );
                fx.setTextColor( Color.parseColor( "#FF696B69" ) );
                wo.setTextColor( Color.parseColor( "#FF696B69" ) );
                Intent intent=new Intent( getContext(), CreateNewsActivity.class );
                intent.putExtra( "user",user );
                startActivity( intent );
                getActivity().overridePendingTransition(0, 0);
                getActivity().finish();
            }
        } );
        fx.setOnClickListener( new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                TextPaint paint_1 = dt.getPaint();
                TextPaint paint = fx.getPaint();
                TextPaint paint_2 = wo.getPaint();
                paint.setFakeBoldText(true);
                paint_1.setFakeBoldText( false );
                paint_2.setFakeBoldText( false );
                fx.setTextColor( Color.parseColor( "#1F7C21") );
                dt.setTextColor( Color.parseColor( "#FF696B69" ) );
                wo.setTextColor( Color.parseColor( "#FF696B69" ) );
                Intent intent=new Intent( getContext(), IndexActivity.class );
                intent.putExtra( "user",user );
                startActivity( intent );
                getActivity().overridePendingTransition(0, 0);
                getActivity().finish();

            }
        }  );
        wo.setOnClickListener(  new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                TextPaint paint_2 = dt.getPaint();
                TextPaint paint_1 = fx.getPaint();
                TextPaint paint = wo.getPaint();
                paint.setFakeBoldText(true);
                paint_1.setFakeBoldText( false );
                paint_2.setFakeBoldText( false );
                wo.setTextColor( Color.parseColor( "#1F7C21") );
                dt.setTextColor( Color.parseColor( "#FF696B69" ) );
                fx.setTextColor( Color.parseColor( "#FF696B69" ));
                Intent intent=new Intent( getContext(), LoginStepActivity.class );
                intent.putExtra( "user",user );
                startActivity( intent );
                getActivity().overridePendingTransition(0, 0);
                getActivity().finish();

            }
        }  );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate( R.layout.fw_bottom,container,false );
        dt=(TextView)view.findViewById( R.id.dt );
        fx=(TextView)view.findViewById( R.id.fx );
        wo=(TextView)view.findViewById( R.id.wo );
        return view;
    }
}
