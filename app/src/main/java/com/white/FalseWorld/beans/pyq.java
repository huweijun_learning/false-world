package com.white.FalseWorld.beans;

/**
 * 作者：white
 * 时间：2020/10/16.
 *
 * 朋友圈字段
 */
public class pyq {
    private int user_id;
    private int pyq_id;
    private int pyq_type;
    private String pyq_text;
    private String pyq_picture_href;
    private String pyq_web_text;
    private String pyq_web_picture;
    private int pyq_like_num;
    private int pyq_day;
    private int pyq_mouth;
    private int pyq_year;
    private String pyq_time;
    private String pyq_place;


    private int zongshu;


    /**
     *
     * pyq_type朋友圈类型分类：
     * 1：纯文本
     * 2：图片+文本
     * 3：网页（图片+文本）
     * 4：文本+网页（图片+文本）
     * @return
     */
    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getPyq_id() {
        return pyq_id;
    }

    public void setPyq_id(int pyq_id) {
        this.pyq_id = pyq_id;
    }

    public String getPyq_text() {
        return pyq_text;
    }

    public void setPyq_text(String pyq_text) {
        this.pyq_text = pyq_text;
    }

    public String getPyq_picture_href() {
        return pyq_picture_href;
    }

    public void setPyq_picture_href(String pyq_picture_href) {
        this.pyq_picture_href = pyq_picture_href;
    }

    public String getPyq_web_text() {
        return pyq_web_text;
    }

    public void setPyq_web_text(String pyq_web_text) {
        this.pyq_web_text = pyq_web_text;
    }

    public String getPyq_web_picture() {
        return pyq_web_picture;
    }

    public void setPyq_web_picture(String pyq_web_picture) {
        this.pyq_web_picture = pyq_web_picture;
    }

    public int getPyq_like_num() {
        return pyq_like_num;
    }

    public void setPyq_like_num(int pyq_like_num) {
        this.pyq_like_num = pyq_like_num;
    }

    public int getPyq_day() {
        return pyq_day;
    }

    public void setPyq_day(int pyq_day) {
        this.pyq_day = pyq_day;
    }

    public int getPyq_mouth() {
        return pyq_mouth;
    }

    public void setPyq_mouth(int pyq_mouth) {
        this.pyq_mouth = pyq_mouth;
    }

    public int getPyq_year() {
        return pyq_year;
    }

    public void setPyq_year(int pyq_year) {
        this.pyq_year = pyq_year;
    }

    public String getPyq_place() {
        return pyq_place;
    }

    public void setPyq_place(String pyq_place) {
        this.pyq_place = pyq_place;
    }

    public String getPyq_time() {
        return pyq_time;
    }

    public void setPyq_time(String pyq_time) {
        this.pyq_time = pyq_time;
    }

    public int getPyq_type() {
        return pyq_type;
    }

    public void setPyq_type(int pyq_type) {
        this.pyq_type = pyq_type;
    }

    public int getZongshu() {
        return zongshu;
    }

    public void setZongshu(int zongshu) {
        this.zongshu = zongshu;
    }
}
