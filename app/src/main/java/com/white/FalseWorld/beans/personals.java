package com.white.FalseWorld.beans;

/**
 * 作者：white
 * 时间：2020/10/1.
 *
 * 用户字段
 */
public class personals {
    private int user_id;
    private String user;
    private String password;
    private String user_wx;
    private String headpicture;
    private int tf_appUpdate;
    private String wx_pyq_label;
    private String wx_pyq_picture;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_wx() {
        return user_wx;
    }

    public void setUser_wx(String user_wx) {
        this.user_wx = user_wx;
    }

    public String getHeadpicture() {
        return headpicture;
    }

    public void setHeadpicture(String headpicture) {
        this.headpicture = headpicture;
    }

    public int getTf_appUpdate() {
        return tf_appUpdate;
    }

    public void setTf_appUpdate(int tf_appUpdate) {
        this.tf_appUpdate = tf_appUpdate;
    }

    public String getWx_pyq_label() {
        return wx_pyq_label;
    }

    public void setWx_pyq_label(String wx_pyq_label) {
        this.wx_pyq_label = wx_pyq_label;
    }

    public String getWx_pyq_picture() {
        return wx_pyq_picture;
    }

    public void setWx_pyq_picture(String wx_pyq_picture) {
        this.wx_pyq_picture = wx_pyq_picture;
    }
}
