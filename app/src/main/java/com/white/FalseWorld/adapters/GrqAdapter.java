package com.white.FalseWorld.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.white.FalseWorld.GrqMainActivity;
import com.white.FalseWorld.R;
import com.white.FalseWorld.beans.pyq;

import java.util.ArrayList;

public class GrqAdapter  extends RecyclerView.Adapter<GrqAdapter.ViewHolder>  {

    private Context mContext;
    private RecyclerView mRecyclerView;
    private ArrayList<pyq> pyqArrayList;

    public GrqAdapter(Context context, RecyclerView recyclerView, ArrayList<pyq> pyqArrayList) {
        this.mContext = context;
        mRecyclerView = recyclerView;
        this.pyqArrayList=pyqArrayList;
    }
    @NonNull
    @Override
    public GrqAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from( mContext ).inflate( R.layout.include_recycler_grq,parent,false );

        final ViewHolder viewHolder=new ViewHolder( view );
        viewHolder.grq_text.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMain(viewHolder);
            }
        } );
        viewHolder.grq_img_text.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMain(viewHolder);
            }
        } );
        viewHolder.grq_imgAndtext_url.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMain(viewHolder);
            }
        } );
        viewHolder.grq_imgAndtextAndtext_url.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startMain(viewHolder);
            }
        } );
        return viewHolder;
    }

    private void startMain(ViewHolder viewHolder) {
        String grq_id=viewHolder.grq_id.getText().toString();
        Intent intent=new Intent( mContext, GrqMainActivity.class );
        intent.putExtra( "grq_id",grq_id );
        mContext.startActivity( intent );
    }

    @Override
    public void onBindViewHolder(@NonNull GrqAdapter.ViewHolder holder, int position) {

        pyq pyq=pyqArrayList.get( position );
        holder.grq_time_day.setText( String.valueOf( pyq.getPyq_day() ) );
        holder.grq_time_mouth.setText( String.valueOf(pyq.getPyq_mouth())+"月" );
        holder.grq_id.setText( String.valueOf(pyq.getPyq_id()) );

        /**
         * 1.判断pyq_type是什么类型
         * 2.根据type类型添加不同的控件内容，以及是否可见
         *
         * pyq_type朋友圈类型分类：
         * 1：纯文本
         * 2：图片+文本
         * 3：网页（图片+文本）
         * 4：文本+网页（图片+文本）
         */
        //System.out.println( Integer.valueOf(pyq.getPyq_type()) );
        if (Integer.valueOf(pyq.getPyq_type())==1){

            holder.grq_text.setVisibility( View.VISIBLE  );
            holder.grq_img_text.setVisibility( View.GONE  );
            holder.grq_imgAndtext_url.setVisibility( View.GONE  );
            holder.grq_imgAndtextAndtext_url.setVisibility( View.GONE  );

            holder.grq_text_text.setText( String.valueOf(pyq.getPyq_text() ));

        }else if (Integer.valueOf(pyq.getPyq_type())==2){
            System.out.println( "第二条朋友圈");
            holder.grq_text.setVisibility( View.GONE  );
            holder.grq_img_text.setVisibility( View.VISIBLE  );
            holder.grq_imgAndtext_url.setVisibility( View.GONE  );
            holder.grq_imgAndtextAndtext_url.setVisibility( View.GONE  );

            holder.grq_img_text_text.setText( String.valueOf(pyq.getPyq_text()) );
            Glide.with(mContext)
                    .asBitmap()
                    .load(String.valueOf(pyq.getPyq_picture_href()))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            Drawable drawable = new BitmapDrawable(resource);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                holder.grq_img_text_picture.setImageDrawable(drawable);
                            }
                        }
                    });
        }else if (Integer.valueOf(pyq.getPyq_type())==3){
            System.out.println( "第三条朋友圈");
            holder.grq_text.setVisibility( View.GONE  );
            holder.grq_img_text.setVisibility( View.GONE  );
            holder.grq_imgAndtext_url.setVisibility( View.VISIBLE  );
            holder.grq_imgAndtextAndtext_url.setVisibility( View.GONE  );

            holder.grq_imgAndtext_url_text.setText( String.valueOf(pyq.getPyq_web_text()) );
            Glide.with(mContext)
                    .asBitmap()
                    .load(String.valueOf(pyq.getPyq_web_picture()))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            Drawable drawable = new BitmapDrawable(resource);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                holder.grq_imgAndtext_url_picture.setImageDrawable(drawable);
                            }
                        }
                    });
        }else if (Integer.valueOf(pyq.getPyq_type())==4){
            System.out.println( "第四条朋友圈");
            holder.grq_text.setVisibility( View.GONE  );
            holder.grq_img_text.setVisibility( View.GONE  );
            holder.grq_imgAndtext_url.setVisibility( View.GONE  );
            holder.grq_imgAndtextAndtext_url.setVisibility( View.VISIBLE  );

            holder.grq_imgAndtextAndtext_url_titletext.setText( String.valueOf(pyq.getPyq_text()) );
            holder.grq_imgAndtextAndtext_url_text.setText( String.valueOf(pyq.getPyq_web_text()) );
            Glide.with(mContext)
                    .asBitmap()
                    .load(String.valueOf(pyq.getPyq_web_picture()))
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                            Drawable drawable = new BitmapDrawable(resource);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                holder.grq_imgAndtextAndtext_url_picture.setImageDrawable(drawable);
                            }
                        }
                    });
        }

    }

    @Override
    public int getItemCount() {
        return pyqArrayList.size();
    }
    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView grq_id,grq_time_day,grq_time_mouth,grq_text_text,grq_img_text_text,grq_imgAndtext_url_text,grq_imgAndtextAndtext_url_titletext,grq_imgAndtextAndtext_url_text;
        ImageView  grq_img_text_picture,grq_imgAndtext_url_picture,grq_imgAndtextAndtext_url_picture;
        LinearLayout grq_text,grq_img_text,grq_imgAndtext_url,grq_imgAndtextAndtext_url;
        /**
         *
         * 初始化布局中的控件
         *
         * */
        public ViewHolder(View itemView){
            super( itemView );
            grq_id=itemView.findViewById( R.id.grq_id );
            grq_time_day=itemView.findViewById( R.id.grq_time_day );
            grq_time_mouth=itemView.findViewById( R.id.grq_time_mouth );
            grq_text_text=itemView.findViewById( R.id.grq_text_text );
            grq_img_text_text=itemView.findViewById( R.id.grq_img_text_text );
            grq_imgAndtext_url_text=itemView.findViewById( R.id.grq_imgAndtext_url_text );
            grq_imgAndtextAndtext_url_titletext=itemView.findViewById( R.id.grq_imgAndtextAndtext_url_titletext );
            grq_imgAndtextAndtext_url_text=itemView.findViewById( R.id.grq_imgAndtextAndtext_url_text );
            grq_img_text_picture=itemView.findViewById( R.id.grq_img_text_picture );
            grq_imgAndtext_url_picture=itemView.findViewById( R.id.grq_imgAndtext_url_picture );
            grq_imgAndtextAndtext_url_picture=itemView.findViewById( R.id.grq_imgAndtextAndtext_url_picture );
            grq_text=itemView.findViewById( R.id.grq_text );
            grq_img_text=itemView.findViewById( R.id.grq_img_text );
            grq_imgAndtext_url=itemView.findViewById( R.id.grq_imgAndtext_url );
            grq_imgAndtextAndtext_url=itemView.findViewById( R.id.grq_imgAndtextAndtext_url );

        }
    }
}
