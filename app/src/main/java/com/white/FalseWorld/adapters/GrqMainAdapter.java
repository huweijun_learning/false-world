package com.white.FalseWorld.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.white.FalseWorld.R;
import com.white.FalseWorld.beans.pyq;
import com.white.FalseWorld.widgets.NineGridView;

import java.util.ArrayList;

public class GrqMainAdapter  extends RecyclerView.Adapter<GrqMainAdapter.ViewHolder>{
/**
 * 作者：white
 * 时间：2020/10/3.
 * 暂前：无作用
 */
    private RequestOptions mRequestOptions;
    private Context mContext;
    private DrawableTransitionOptions mDrawableTransitionOptions;
    private ArrayList<pyq> pyqArrayList;
    private RecyclerView mRecyclerView;

    public GrqMainAdapter(Context context, RecyclerView recyclerView, ArrayList<pyq> pyqArrayList) {
        this.mContext = context;
        mRecyclerView = recyclerView;
        this.mRequestOptions = new RequestOptions().centerCrop();
        this.mDrawableTransitionOptions = DrawableTransitionOptions.withCrossFade();
        this.pyqArrayList=pyqArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        pyq pyq=pyqArrayList.get( position );


    }



    @Override
    public int getItemCount() {
        return pyqArrayList.size();
    }
    static class ViewHolder extends RecyclerView.ViewHolder {

        private NineGridView nineGridView;

        public ViewHolder(@NonNull View itemView) {
            super( itemView );
            nineGridView=itemView.findViewById( R.id.grqmain_textAndimg );
        }
    }
}
