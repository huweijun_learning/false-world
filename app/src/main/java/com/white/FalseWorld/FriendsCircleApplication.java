package com.white.FalseWorld;

import android.app.Application;
import android.content.Context;

import com.white.FalseWorld.others.DataCenter;

/**
 * 作者：white
 * 时间：2020/10/1.
 */
public class FriendsCircleApplication extends Application {

    public static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        DataCenter.init();
    }
}
