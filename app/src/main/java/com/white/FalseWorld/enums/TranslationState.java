package com.white.FalseWorld.enums;

public enum TranslationState {
    START, CENTER, END
}
