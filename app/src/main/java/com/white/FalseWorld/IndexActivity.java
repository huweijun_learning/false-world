package com.white.FalseWorld;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.gyf.immersionbar.ImmersionBar;
import com.white.FalseWorld.fragment.FW_BottomFragement;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import static java.sql.DriverManager.getConnection;

public class IndexActivity extends AppCompatActivity {

    private TextView wx_me_username,wx_me_id;
    private LinearLayout grq,pyq,setting,LoginStep;
    private ImageView wx_me_headpicture;

    String wx_id;
    private String temp_user,temp_headpicture;

    FW_BottomFragement fw_bottomFragement=new FW_BottomFragement();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_index );
        wx_id = getIntent().getStringExtra("user");
        ImmersionBar.with(this).init();//一键透明状态栏

        wx_me_username=findViewById( R.id.wx_me_username );
        wx_me_id=findViewById( R.id.wx_me_id );
        wx_me_headpicture=findViewById( R.id.wx_me_headpicture );
        grq=findViewById( R.id.grq );
        pyq=findViewById( R.id.pyq );
        setting=findViewById( R.id.setting );
        LoginStep=findViewById( R.id.LoginStep );

        index_query_mysql();
        grq.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent( IndexActivity.this,GrqActivity.class );
                intent.putExtra( "user",wx_id );
                startActivity( intent );
            }
        } );
        pyq.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent( IndexActivity.this,MainActivity.class );
                intent.putExtra( "user",wx_id );
                startActivity( intent );
            }
        } );
        setting.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent( IndexActivity.this,SettingActivity.class );
                intent.putExtra( "user",wx_id );
                startActivity( intent );
            }
        } );
        LoginStep.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent( IndexActivity.this,LoginStepActivity.class );
                intent.putExtra( "user",wx_id );
                startActivity( intent );
            }
        } );
        replaceFragment_bottom( fw_bottomFragement );
    }
    private void replaceFragment_bottom(FW_BottomFragement fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace( R.id.fw_bottomframe, fragment );
        fragmentTransaction.commit();

    }
    private void index_query_mysql() {
        new Thread( new Runnable() {
            @Override
            public void run() {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                    Connection cn= getConnection( getString( R.string.mysql_address ),getString( R.string.mysql_username ),getString( R.string.mysql_password ));
                    System.out.println("连接数据库成功");
                    String sql="SELECT * FROM personals WHERE user_wx='"+wx_id+"'";
                    Statement st=(Statement)cn.createStatement();
                    ResultSet rs=st.executeQuery(sql);
                    while(rs.next()){
                        temp_user=rs.getString("user");
                        temp_headpicture=rs.getString( "headpicture" );
                    }
                    cn.close();
                    st.close();
                    rs.close();
                    Message msg = new Message();
                    Bundle data = new Bundle();
                    data.putString("user", temp_user);
                    data.putString("headpicture",getString( R.string.url_prefix )+temp_headpicture);
                    msg.setData(data);
                    msg.what = 1;
                    handler.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } ).start();
    }
    //因为mysql在新线程中运行，所以得重新回到UI线程进行组件内容的修改等操作
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                Bundle data = msg.getData();
                String user = data.getString("user");
                String headpicture = data.getString("headpicture");
                wx_me_username.setText( user );
                wx_me_id.setText("微信号："+ wx_id );
                Glide.with(getApplicationContext())
                        .asBitmap()
                        .load(headpicture )
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                                Drawable drawable = new BitmapDrawable(resource);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    wx_me_headpicture.setImageDrawable(drawable);
                                }
                            }

                        });
            } else if (msg.what ==0) {
                Toast.makeText(getApplicationContext(),"请求资源不成功",Toast.LENGTH_LONG).show();
            }
        }
    };
}
