package com.white.FalseWorld.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;
/**
 * 作者：white
 * 时间：2020/10/5.
 */
public class ShowDateOrTimePickerDialog {

    public static void showDatePickerDialog(Activity activity, int themeResId, final TextView tv, Calendar calendar) {
        /**
         * 直接创建一个DatePickerDialog对话框实例，并将它显示出来
         * 传参数：1.content，2.主题类型，3.绑定监听器，4.设置初始日期
         */
        new DatePickerDialog(activity,themeResId
                ,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                //得到选择的时间
                tv.setText( year + "年" + (monthOfYear+1) + "月" + dayOfMonth + "日");
            }
        }
                ,calendar.get(Calendar.YEAR)
                ,calendar.get(Calendar.MONTH)
                ,calendar.get(Calendar.DAY_OF_MONTH)).show();
    }
    public static void showTimePickerDialog(Activity activity,int themeResId, final TextView tv, Calendar calendar) {
        /**
         * 直接创建一个TimePickerDialog对话框实例，并将它显示出来
         * 传参数：1.content，2.主题类型，3.绑定监听器，4.设置初始日期
         */
        new TimePickerDialog( activity,themeResId,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view,
                                          int hourOfDay, int minute) {
                        tv.setText( hourOfDay + ":" + minute);
                    }
                }
                , calendar.get(Calendar.HOUR_OF_DAY)
                , calendar.get(Calendar.MINUTE)
                ,true).show();// true表示采用24小时制
    }
}
