
# FalseWorld（开源率100%）

#### 介绍

- FalseWorld（FW，微世界）开源Android APP，提供多种玩法，让你在聊天中拥有更多话题等功能，可二次开发。
- 注意：仅供娱乐！！！
-  **详情（包含apk地址）：https://mp.weixin.qq.com/s/oflfrlNj2ZiKqhxiRn_rOw** 
- apk下载地址：https://wwa.lanzous.com/iPCAshj87de


#### 集成工具
项目中用到的框架以及其他项目地址如下：

- 朋友圈：https://github.com/imicrason/HighPerformanceFriendsCircle
- 沉浸式状态栏：'com.gyf.immersionbar:immersionbar:3.0.0'
- 相册多选：'com.github.LuckSiege.PictureSelector:picture_library:v2.5.9'
- ...

#### 功能展示
|![动态](https://images.gitee.com/uploads/images/2020/1016/122639_c1a63628_7724819.jpeg "动态.jpg")|![发现](https://images.gitee.com/uploads/images/2020/1016/122656_ee393663_7724819.jpeg "发现.jpg")|![个人圈](https://images.gitee.com/uploads/images/2020/1016/122712_fa1dc800_7724819.jpeg "个人圈.jpg")|
|--|--|--|
|![朋友圈](https://images.gitee.com/uploads/images/2020/1016/123004_1ee2a1ae_7724819.jpeg "朋友圈.jpg")|![详情](https://images.gitee.com/uploads/images/2020/1016/123101_5d561fc6_7724819.jpeg "详情.jpg")|![设置](https://images.gitee.com/uploads/images/2020/1016/123228_3670f915_7724819.jpeg "设置.jpg")|





#### 数据库
在项目中，数据库的增删改查以及相关字段也进行公布。以下为展示的两个表

```
CREATE TABLE `personals` (
  `user_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `user` varchar(100) NOT NULL COMMENT '用户账号',
  `password` varchar(100) NOT NULL COMMENT '用户密码',
  `user_wx` varchar(100) DEFAULT NULL COMMENT '用户微信号',
  `headpicture` varchar(255) DEFAULT NULL COMMENT '用户头像地址',
  `tf_appUpdate` int(11) DEFAULT NULL COMMENT '用户是否更新APP',
  `wx_pyq_label` varchar(255) DEFAULT NULL COMMENT '用户朋友圈标签',
  `wx_pyq_picture` varchar(255) DEFAULT NULL COMMENT '用户朋友圈背景图片地址',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8
```



```
CREATE TABLE `pyq` (
  `user_id` int(255) NOT NULL COMMENT '用户id',
  `pyq_id` int(255) NOT NULL AUTO_INCREMENT COMMENT '朋友圈id',
  `pyq_type` int(10) DEFAULT NULL COMMENT '朋友圈类型',
  `pyq_text` varchar(255) DEFAULT NULL COMMENT '朋友圈文本',
  `pyq_picture_href` varchar(255) DEFAULT NULL COMMENT '朋友圈图片地址',
  `pyq_web_text` varchar(255) DEFAULT NULL COMMENT '朋友圈分享网页文本',
  `pyq_web_picture` varchar(255) DEFAULT NULL COMMENT '朋友圈分享网页图片地址',
  `pyq_like_num` int(11) DEFAULT NULL COMMENT '朋友圈点赞人数',
  `pyq_day` int(31) DEFAULT NULL COMMENT '朋友圈发布时间(日)',
  `pyq_mouth` int(12) DEFAULT NULL COMMENT '朋友圈发布时间(月)',
  `pyq_year` int(11) DEFAULT NULL COMMENT '朋友圈发布时间(年)',
  `pyq_time` varchar(255) DEFAULT NULL COMMENT '朋友圈发布时间',
  `pyq_place` varchar(255) DEFAULT NULL COMMENT '朋友圈发布地址',
  KEY `pyq_id` (`pyq_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8
```

>>>>>>> 71f5889052806dbcfcc9a7b7ad65935d8de5d43a
